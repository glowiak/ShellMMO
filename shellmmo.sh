#!/bin/sh
# ShellMMO

# load configs
HERE=$(readlink -f $(dirname $0))
. $HERE/conf/locations.conf
. $HERE/conf/libs.conf
. $HERE/conf/costs.conf

# get names from xml
SELF=$($XML Name $HERE/conf/program.xml)
SELF_VER=$($XML Version $HERE/conf/program.xml)
SELF_CODER=$($XML Coder $HERE/conf/program.xml)
SELF_WWW=$($XML Website $HERE/conf/program.xml)
SELF_LANG=$($XML Language $HERE/conf/program.xml)

# set the language

. $LANGDIR/$SELF_LANG.lang

# startup

echo "$LANG_START $SELF..."
echo "$LANG_VERSION: $SELF_VER"
echo "$LANG_MANF: $SELF_CODER"
echo "$LANG_WWW: $SELF_WWW"

# functions

ask_nick()
{
	clear
	echo -n "$LANG_NICKNAME_QUESTION: "
	read playerNick
}

MainMenu()
{
	clear
	cat $ARTDIR/logo.txt
	echo "-------------------------------------------------------------"
	echo "${LANG_WELCOME}, ${playerNick}!"
	echo "$LANG_OPTIONS:"
	echo "--> $LANG_OPT_NEWGAME" # new game (1)
	echo "--> $LANG_OPT_LOADGAME" # load game (2)
	echo "--> $LANG_OPT_SETTINGS" # settings (3)
	echo "--> $LANG_OPT_ABOUT" # about (4)
	echo "--> $LANG_OPT_EXIT" # exit (5)
	echo ""
	echo -n "$LANG_Q_OPT (1-5): "
	read OptionM0
	case $OptionM0 in
		1)
			NewGame_ASKNAME
			;;
		2)
			LoadGame_SELECT
			;;
		3)
			SettingsMenu
			;;
		4)
			About
			;;
		5)
			$WV_CLEAR
			exit 0
			;;
		*)
			MainMenu
			;;
	esac
}
LoadGame_SELECT()
{
	clear
	echo -n "Enter the path of your saved game: "
	read loader
	if [ ! -f "$loader" ]
	then
		echo "Incorrect save!"
		sleep 1
		MainMenu
	fi
	. $loader
	export ORG_GAME_CURRENTGAME=$loader
	Game_MainMenu
}
About()
{
	clear
	cat $ARTDIR/logo.txt
	echo "-------------------------------------------------------------"
	echo "$SELF $LANG_VERSION $SELF_VER"
	echo "$LANG_MANF: $SELF_CODER"
	echo "$LANG_WWW: $SELF_WWW"
	echo "$LANG_ABOUT_TRL"
	echo ""
	echo "$LANG_MEEXIT..."
	read saruman
	MainMenu
}
NewGame_ASKNAME()
{
	clear
	echo -n "$LANG_Q_NEWGAME: "
	read NewGameName
	if [ -f "$SAVEDIR/${NewGameName}.smg" ]
	then
		echo "Save already exists!"
		sleep 1
		NewGame_ASKNAME
	fi
	echo "$LANG_CREATING_GAME $NewGameName..."
	cat > $SAVEDIR/${NewGameName}.smg << "EOF"
# ShellMMO Saved Game (SMG)
EOF
	echo "GSE_NAME=${NewGameName}" >> $SAVEDIR/$NewGameName.smg
	echo "GSE_CREATOR=${playerNick}" >> $SAVEDIR/$NewGameName.smg
	cat >> $SAVEDIR/$NewGameName.smg << "EOF"
GSE_RSC_WOOD=100
GSE_RSC_STONE=100
GSE_RSC_IRON=100
GSE_RSC_MONEY=100
GSE_WARRIOR=0
GSE_PEOPLE=20
GSE_BUILDING_SAWMILL=0
GSE_BUILDING_STONEPIT=0
GSE_BUILDING_IRONMINE=0
GSE_BUILDING_BARRACK=0
GSE_BUILDING_MARKET=0
GSE_CITIES=0
EOF
	. $SAVEDIR/$NewGameName.smg
	export ORG_GAME_CURRENTGAME=$SAVEDIR/$NewGameName.smg
	Game_MainMenu
}


Game_MainMenu()
{
	clear
	echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
	echo ""
	echo "$LANG_OPTIONS:"
	echo "--> $LANG_COLLECT" # collect resources (1)
	echo "--> $LANG_BUILD" # build buildings (2)
	echo "--> $LANG_HIRE" # hire warriors (3)
	echo "--> $LANG_TRADE" # trade (4)
	echo "--> $LANG_CONQUER" # conquer (5)
	echo "--> $LANG_QUIT_MENU" # quit to menu (6)
	echo ""
	echo -n "$LANG_Q_OPT (1-6): "
	read OptionM1
	case $OptionM1 in
		1)
			GameCollect
			;;
		2)
			GameBuild
			;;
		3)
			GameBarracks
			;;
		4)
			GameMarket
			;;
		5)
			JuliuszCezar
			;;
		6)
			MainMenu
			;;
		*)
			WriteSave $ORG_GAME_CURRENTGAME
			Game_MainMenu
			;;
	esac
}
WriteSave()
{
	MSAVE=$1
	rm -f $MSAVE
	echo "# ShellMMO Saved Game (SMG)" > $MSAVE
	echo "GSE_RSC_WOOD=$GSE_RSC_WOOD" >> $MSAVE
	echo "GSE_RSC_STONE=$GSE_RSC_STONE" >> $MSAVE
	echo "GSE_RSC_IRON=$GSE_RSC_IRON" >> $MSAVE
	echo "GSE_RSC_MONEY=$GSE_RSC_MONEY" >> $MSAVE
	echo "GSE_WARRIOR=$GSE_WARRIOR" >> $MSAVE
	echo "GSE_PEOPLE=$GSE_PEOPLE" >> $MSAVE
	echo "GSE_BUILDING_SAWMILL=$GSE_BUILDING_SAWMILL" >> $MSAVE
	echo "GSE_BUILDING_STONEPIT=$GSE_BUILDING_STONEPIT" >> $MSAVE
	echo "GSE_BUILDING_IRONMINE=$GSE_BUILDING_IRONMINE" >> $MSAVE
	echo "GSE_BUILDING_BARRACK=$GSE_BUILDING_BARRACK" >> $MSAVE
	echo "GSE_BUILDING_MARKET=$GSE_BUILDING_MARKET" >> $MSAVE
	echo "GSE_CITIES=$GSE_CITIES" >> $MSAVE
	
}
GameBarracks()
{
	clear
	echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
	echo "##### $LANG_WARHIRE #####"
	echo "$LANG_Q_OPT"
}
GameCollect()
{
	clear
	echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
	echo "####### $LANG_COLLECT #######"
	echo "$LANG_Q_OPT"
	echo "--> $LANG_CLT_WOOD10" # 10 woods (1)
	echo "--> $LANG_CLT_STONE10" # 10 stones (2)
	echo "--> $LANG_CLT_IRON10" # 10 irons (3)
	echo "--> $LANG_CLT_WARRIOR10" # 10 warriors (4)
	echo "--> $LANG_CLT_TAXES" # collect taxes (5)
	echo "--> $LANG_QUIT_MENU " # quit to menu (6)
	echo ""
	echo -n "$LANG_Q_OPT (1-5): "
	read OptionM3
	case $OptionM3 in
		1)
			GameCollect_Wood
			;;
		2)
			GameCollect_Stone
			;;
		3)
			GameCollect_Iron
			;;
		4)
			GameCollect_Warriors
			;;
		5)
			GameCollect_Taxes
			;;
		*)
			Game_MainMenu
			;;
	esac
}
GameCollect_Wood()
{
	clear
	echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
	echo ""
	if [ "$GSE_BUILDING_SAWMILL" == "0" ]
	then
		echo "$LANG_NEED_BLD_SAWMILL"
		sleep 1
		Game_MainMenu
	fi
	echo "$LANG_ABOUT_SAWMILL..."
	echo ""
	echo "$LANG_PLEASE_WAIT ${BTIME_WOOD10} $LANG_SECONDS..."
	sleep ${BTIME_WOOD10}
	export GSE_RSC_WOOD=$(expr $GSE_RSC_WOOD + 10)
	Game_MainMenu
}
GameCollect_Stone()
{
	clear
	echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
	echo ""
	if [ "$GSE_BUILDING_STONEPIT" == "0" ]
	then
		echo "$LANG_NEED_BLD_STONEPIT"
		sleep 1
		Game_MainMenu
	fi
	echo "$LANG_ABOUT_STONE..."
	echo ""
	echo "$LANG_PLEASE_WAIT ${BTIME_STONE10} $LANG_SECONDS..."
	sleep ${BTIME_STONE10}
	export GSE_RSC_STONE=$(expr $GSE_RSC_STONE + 10)
	Game_MainMenu
}
GameCollect_Iron()
{
	clear
	echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
	echo ""
	if [ "$GSE_BUILDING_IRONMINE" == "0" ]
	then
		echo "$LANG_NEED_BLD_IRONMINE"
		sleep 1
		Game_MainMenu
	fi
	echo "$LANG_ABOUT_IRON..."
	echo ""
	echo "$LANG_PLEASE_WAIT ${BTIME_IRON10} $LANG_SECONDS..."
	sleep ${BTIME_IRON10}
	export GSE_RSC_IRON=$(expr $GSE_RSC_IRON + 10)
	Game_MainMenu
}
GameCollect_Taxes()
{
	clear
	echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
	echo ""
	echo "$LANG_ABOUT_TAXES..."
	echo ""
	echo "$LANG_PLEASE_WAIT ${BTIME_TAXES35} $LANG_SECONDS..."
	sleep ${BTIME_TAXES35}
	export GSE_RSC_MONEY=$(expr $GSE_RSC_MONEY + 35)
	Game_MainMenu
}
GameBuild()
{
	clear
	echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
	echo "####### $LANG_BUILD ######"
	echo "$LANG_Q_OPT:"
	echo "--> $LANG_BLD_SAWMILL" # sawmill/tartak (1)
	echo "--> $LANG_BLD_STONEPIT" # stonepit/kamieniołom (2)
	echo "--> $LANG_BLD_IRONMINE" # iron mine/kopalnia żelaza (3)
	echo "--> $LANG_BLD_BARRACK" # barracks/koszary (4)
	echo "--> $LANG_BLD_MARKET" # market/targ (5)
	echo "--> $LANG_BLD_HOUSE" # house/dom (6)
	echo "--> $LANG_QUIT_MENU" # quit to menu (7)
	echo ""
	echo -n "$LANG_Q_OPT (1-7): "
	read OptionM2
	case $OptionM2 in
		1)
			GameBuild_Sawmill
			;;
		2)
			GameBuild_Stonepit
			;;
		3)
			GameBuild_Ironmine
			;;
		4)
			GameBuild_Barrack
			;;
		5)
			GameBuild_Market
			;;
		6)
			GameBuild_House
			;;
		7)
			Game_MainMenu
			;;
		*)
			GameBuild
			;;
	esac
}
GameBuild_Sawmill()
{
	if [ "$GSE_BUILDING_SAWMILL" == "1" ]
	then
		Game_MainMenu # drop to menu when already built
	fi
	if [ "$GSE_BUILDING_SAWMILL" == "0" ]
	then
		clear
		echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
		echo "$LANG_Q_BUILD ${LANG_B_SAWMILL}?"
		echo "$LANG_STONE: $COSTS_SAWMILL_STONE"
		echo "$LANG_WOOD: $COSTS_SAWMILL_WOOD"
		echo "$LANG_IRON: $COSTS_SAWMILL_IRON"
		echo "$LANG_MONEY: $COSTS_SAWMILL_MONEY"
		echo "$LANG_BTIME: $BTIME_SAWMILL $LANG_SECONDS"
		echo -n "$LANG_Q_BUILD ${LANG_B_SAWMILL}? [y/n] "
		read GB_Sawmill
		case $GB_Sawmill in
			y)
				# fall to menu if not enough resources
				if [ "$GSE_RSC_STONE" < "COSTS_SAWMILL_STONE" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_WOOD" < "$COSTS_SAWMILL_WOOD" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_IRON" < "$COSTS_SAWMILL_IRON" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_MONEY" < "$COSTS_SAWMILL_MONEY" ]
				then
					Game_MainMenu
				fi
				# decrease resources count
				if [ "$COSTS_SAWMILL_STONE" > 0 ]
				then
					export GSE_RSC_STONE=$(expr $GSE_RSC_STONE - $COSTS_SAWMILL_STONE)
				fi
				if [ "$COSTS_SAWMILL_WOOD" > 0 ]
				then
					export GSE_RSC_WOOD=$(expr $GSE_RSC_WOOD - $COSTS_SAWMILL_WOOD)
				fi
				if [ "$COSTS_SAWMILL_IRON" > 0 ]
				then
					export GSE_RSC_IRON=$(expr $GSE_RSC_IRON - $COSTS_SAWMILL_IRON)
				fi
				if [ "$COSTS_SAWMILL_MONEY" > 0 ]
				then
					export GSE_RSC_MONEY=$(expr $GSE_RSC_MONEY - $COSTS_SAWMILL_MONEY)
				fi
				clear
				echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
				echo ""
				echo "$LANG_PLEASE_WAIT $BTIME_SAWMILL $LANG_SECONDS..."
				sleep $BTIME_SAWMILL
				export GSE_BUILDING_SAWMILL=1
				Game_MainMenu
				;;
			*)
				Game_MainMenu
				;;
		esac
	fi
}
GameBuild_Stonepit()
{
	if [ "$GSE_BUILDING_STONEPIT" == "1" ]
	then
		Game_MainMenu # drop to menu when already built
	fi
	if [ "$GSE_BUILDING_STONEPIT" == "0" ]
	then
		clear
		echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
		echo "$LANG_Q_BUILD ${LANG_B_STONEPIT}?"
		echo "$LANG_STONE: $COSTS_STONEPIT_STONE"
		echo "$LANG_WOOD: $COSTS_STONEPIT_WOOD"
		echo "$LANG_IRON: $COSTS_STONEPIT_IRON"
		echo "$LANG_MONEY: $COSTS_STONEPIT_MONEY"
		echo "$LANG_BTIME: $BTIME_STONEPIT $LANG_SECONDS"
		echo -n "$LANG_Q_BUILD ${LANG_B_STONEPIT}? [y/n] "
		read GB_Stonepit
		case $GB_Stonepit in
			y)
				# fall to menu if not enough resources
				if [ "$GSE_RSC_STONE" < "COSTS_STONEPIT_STONE" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_WOOD" < "$COSTS_STONEPIT_WOOD" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_IRON" < "$COSTS_STONEPIT_IRON" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_MONEY" < "$COSTS_STONEPIT_MONEY" ]
				then
					Game_MainMenu
				fi
				# decrease resources count
				if [ "$COSTS_STONEPIT_STONE" > 0 ]
				then
					export GSE_RSC_STONE=$(expr $GSE_RSC_STONE - $COSTS_STONEPIT_STONE)
				fi
				if [ "$COSTS_STONEPIT_WOOD" > 0 ]
				then
					export GSE_RSC_WOOD=$(expr $GSE_RSC_WOOD - $COSTS_STONEPIT_WOOD)
				fi
				if [ "$COSTS_STONEPIT_IRON" > 0 ]
				then
					export GSE_RSC_IRON=$(expr $GSE_RSC_IRON - $COSTS_STONEPIT_IRON)
				fi
				if [ "$COSTS_STONEPIT_MONEY" > 0 ]
				then
					export GSE_RSC_MONEY=$(expr $GSE_RSC_MONEY - $COSTS_STONEPIT_MONEY)
				fi
				clear
				echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
				echo ""
				echo "$LANG_PLEASE_WAIT $BTIME_STONEPIT $LANG_SECONDS..."
				sleep $BTIME_STONEPIT
				export GSE_BUILDING_STONEPIT=1
				Game_MainMenu
				;;
			*)
				Game_MainMenu
				;;
		esac
	fi
}
GameBuild_Ironmine()
{
	if [ "$GSE_BUILDING_IRONMINE" == "1" ]
	then
		Game_MainMenu # drop to menu when already built
	fi
	if [ "$GSE_BUILDING_IRONMINE" == "0" ]
	then
		clear
		echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
		echo "$LANG_Q_BUILD ${LANG_B_IRONMINE}?"
		echo "$LANG_STONE: $COSTS_IRONMINE_STONE"
		echo "$LANG_WOOD: $COSTS_IRONMINE_WOOD"
		echo "$LANG_IRON: $COSTS_IRONMINE_IRON"
		echo "$LANG_MONEY: $COSTS_IRONMINE_MONEY"
		echo "$LANG_BTIME: $BTIME_IRONMINE $LANG_SECONDS"
		echo -n "$LANG_Q_BUILD ${LANG_B_IRONMINE}? [y/n] "
		read GB_Ironmine
		case $GB_Ironmine in
			y)
				# fall to menu if not enough resources
				if [ "$GSE_RSC_STONE" < "COSTS_IRONMINE_STONE" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_WOOD" < "$COSTS_IRONMINE_WOOD" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_IRON" < "$COSTS_IRONMINE_IRON" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_MONEY" < "$COSTS_IRONMINE_MONEY" ]
				then
					Game_MainMenu
				fi
				# decrease resources count
				if [ "$COSTS_IRONMINE_STONE" > 0 ]
				then
					export GSE_RSC_STONE=$(expr $GSE_RSC_STONE - $COSTS_IRONMINE_STONE)
				fi
				if [ "$COSTS_IRONMINE_WOOD" > 0 ]
				then
					export GSE_RSC_WOOD=$(expr $GSE_RSC_WOOD - $COSTS_IRONMINE_WOOD)
				fi
				if [ "$COSTS_IRONMINE_IRON" > 0 ]
				then
					export GSE_RSC_IRON=$(expr $GSE_RSC_IRON - $COSTS_IRONMINE_IRON)
				fi
				if [ "$COSTS_IRONMINE_MONEY" > 0 ]
				then
					export GSE_RSC_MONEY=$(expr $GSE_RSC_MONEY - $COSTS_IRONMINE_MONEY)
				fi
				clear
				echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
				echo ""
				echo "$LANG_PLEASE_WAIT $BTIME_IRONMINE $LANG_SECONDS..."
				sleep $BTIME_IRONMINE
				export GSE_BUILDING_IRONMINE=1
				Game_MainMenu
				;;
			*)
				Game_MainMenu
				;;
		esac
	fi
}
GameBuild_Barrack()
{
	if [ "$GSE_BUILDING_BARRACK" == "1" ]
	then
		Game_MainMenu # drop to menu when already built
	fi
	if [ "$GSE_BUILDING_BARRACK" == "0" ]
	then
		clear
		echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
		echo "$LANG_Q_BUILD ${LANG_B_BARRACK}?"
		echo "$LANG_STONE: $COSTS_BARRACK_STONE"
		echo "$LANG_WOOD: $COSTS_BARRACK_WOOD"
		echo "$LANG_IRON: $COSTS_BARRACK_IRON"
		echo "$LANG_MONEY: $COSTS_BARRACK_MONEY"
		echo "$LANG_BTIME: $BTIME_BARRACK $LANG_SECONDS"
		echo -n "$LANG_Q_BUILD ${LANG_B_BARRACK}? [y/n] "
		read GB_Barrack
		case $GB_Barrack in
			y)
				# fall to menu if not enough resources
				if [ "$GSE_RSC_STONE" < "COSTS_BARRACK_STONE" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_WOOD" < "$COSTS_BARRACK_WOOD" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_IRON" < "$COSTS_BARRACK_IRON" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_MONEY" < "$COSTS_BARRACK_MONEY" ]
				then
					Game_MainMenu
				fi
				# decrease resources count
				if [ "$COSTS_BARRACK_STONE" > 0 ]
				then
					export GSE_RSC_STONE=$(expr $GSE_RSC_STONE - $COSTS_BARRACK_STONE)
				fi
				if [ "$COSTS_BARRACK_WOOD" > 0 ]
				then
					export GSE_RSC_WOOD=$(expr $GSE_RSC_WOOD - $COSTS_BARRACK_WOOD)
				fi
				if [ "$COSTS_BARRACK_IRON" > 0 ]
				then
					export GSE_RSC_IRON=$(expr $GSE_RSC_IRON - $COSTS_BARRACK_IRON)
				fi
				if [ "$COSTS_BARRACK_MONEY" > 0 ]
				then
					export GSE_RSC_MONEY=$(expr $GSE_RSC_MONEY - $COSTS_BARRACK_MONEY)
				fi
				clear
				echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
				echo ""
				echo "$LANG_PLEASE_WAIT $BTIME_BARRACK $LANG_SECONDS..."
				sleep $BTIME_BARRACK
				export GSE_BUILDING_BARRACK=1
				Game_MainMenu
				;;
			*)
				Game_MainMenu
				;;
		esac
	fi
}
GameBuild_Market()
{
	if [ "$GSE_BUILDING_MARKET" == "1" ]
	then
		Game_MainMenu # drop to menu when already built
	fi
	if [ "$GSE_BUILDING_MARKET" == "0" ]
	then
		clear
		echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
		echo "$LANG_Q_BUILD ${LANG_B_MARKET}?"
		echo "$LANG_STONE: $COSTS_MARKET_STONE"
		echo "$LANG_WOOD: $COSTS_MARKET_WOOD"
		echo "$LANG_IRON: $COSTS_MARKET_IRON"
		echo "$LANG_MONEY: $COSTS_MARKET_MONEY"
		echo "$LANG_BTIME: $BTIME_MARKET $LANG_SECONDS"
		echo -n "$LANG_Q_BUILD ${LANG_B_MARKET}? [y/n] "
		read GB_Market
		case $GB_Market in
			y)
				# fall to menu if not enough resources
				if [ "$GSE_RSC_STONE" < "COSTS_MARKET_STONE" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_WOOD" < "$COSTS_MARKET_WOOD" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_IRON" < "$COSTS_MARKET_IRON" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_MONEY" < "$COSTS_MARKET_MONEY" ]
				then
					Game_MainMenu
				fi
				# decrease resources count
				if [ "$COSTS_MARKET_STONE" > 0 ]
				then
					export GSE_RSC_STONE=$(expr $GSE_RSC_STONE - $COSTS_MARKET_STONE)
				fi
				if [ "$COSTS_MARKET_WOOD" > 0 ]
				then
					export GSE_RSC_WOOD=$(expr $GSE_RSC_WOOD - $COSTS_MARKET_WOOD)
				fi
				if [ "$COSTS_MARKET_IRON" > 0 ]
				then
					export GSE_RSC_IRON=$(expr $GSE_RSC_IRON - $COSTS_MARKET_IRON)
				fi
				if [ "$COSTS_MARKET_MONEY" > 0 ]
				then
					export GSE_RSC_MONEY=$(expr $GSE_RSC_MONEY - $COSTS_MARKET_MONEY)
				fi
				clear
				echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
				echo ""
				echo "$LANG_PLEASE_WAIT $BTIME_MARKET $LANG_SECONDS..."
				sleep $BTIME_SAWMILL
				export GSE_BUILDING_MARKET=1
				Game_MainMenu
				;;
			*)
				Game_MainMenu
				;;
		esac
	fi
}
GameBuild_House()
{
	if [ "$GSE_BUILDING_HOUSE" == "1" ]
	then
		Game_MainMenu # drop to menu when already built
	fi
	if [ "$GSE_BUILDING_HOUSE" == "0" ]
	then
		clear
		echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
		echo "$LANG_Q_BUILD ${LANG_B_HOUSE}?"
		echo "$LANG_STONE: $COSTS_HOUSE_STONE"
		echo "$LANG_WOOD: $COSTS_HOUSE_WOOD"
		echo "$LANG_IRON: $COSTS_HOUSE_IRON"
		echo "$LANG_MONEY: $COSTS_HOUSE_MONEY"
		echo "$LANG_BTIME: $BTIME_HOUSE $LANG_SECONDS"
		echo -n "$LANG_Q_BUILD ${LANG_B_HOUSE}? [y/n] "
		read GB_House
		case $GB_House in
			y)
				# fall to menu if not enough resources
				if [ "$GSE_RSC_STONE" < "COSTS_HOUSE_STONE" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_WOOD" < "$COSTS_HOUSE_WOOD" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_IRON" < "$COSTS_HOUSE_IRON" ]
				then
					Game_MainMenu
				fi
				if [ "$GSE_RSC_MONEY" < "$COSTS_HOUSE_MONEY" ]
				then
					Game_MainMenu
				fi
				# decrease resources count
				if [ "$COSTS_HOUSE_STONE" > 0 ]
				then
					export GSE_RSC_STONE=$(expr $GSE_RSC_STONE - $COSTS_HOUSE_STONE)
				fi
				if [ "$COSTS_HOUSE_WOOD" > 0 ]
				then
					export GSE_RSC_WOOD=$(expr $GSE_RSC_WOOD - $COSTS_HOUSE_WOOD)
				fi
				if [ "$COSTS_HOUSE_IRON" > 0 ]
				then
					export GSE_RSC_IRON=$(expr $GSE_RSC_IRON - $COSTS_HOUSE_IRON)
				fi
				if [ "$COSTS_HOUSE_MONEY" > 0 ]
				then
					export GSE_RSC_MONEY=$(expr $GSE_RSC_MONEY - $COSTS_HOUSE_MONEY)
				fi
				clear
				echo "# ${GSE_CREATOR}$LANG_WHOSEP $LANG_CITY ### $GSE_NAME ### $LANG_WOOD: $GSE_RSC_WOOD ### $LANG_STONE: $GSE_RSC_STONE ### $LANG_IRON: $GSE_RSC_IRON ### $LANG_MONEY: $GSE_RSC_MONEY ### $LANG_WARRIOR: $GSE_WARRIOR ### $LANG_PEOPLE: $GSE_PEOPLE #"
				echo ""
				echo "$LANG_PLEASE_WAIT $BTIME_HOUSE $LANG_SECONDS..."
				sleep $BTIME_HOUSE
				export GSE_PEOPLE=$(expr $GSE_PEOPLE + 5)
				Game_MainMenu
				;;
			*)
				Game_MainMenu
				;;
		esac
	fi
}

# start the Game
ask_nick
MainMenu
