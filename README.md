# UPDATE 2022-04-23

I will not release updates to ShellMMO anymore, it will be replaced by a graphical game Imperialist in short future

### ShellMMO

A strategy game written in Shell

### Why

Recently I started playing a game called Tribal Wars 2. It has very simple mechanics, but the graphical ui is so bloated. So I decided to write a similar game in just Shell (not dash hahaha)

### Please

have on mind that it's unfinished and may be buggy

### Dependiences

a posix-complaint shell like ksh or bash (it will probably not work with dash)

### Translations

You can set translations by editing conf/program.xml file in the Language field.

Available languages:

	English
	Polski

### Roadmap

[DONE] get working menus

[DONE] building and collecting

[DONE] translations

[DONE] costs resolving

[DONE] saving and loading

[WIP] settings and configuring

[WIP] in-game language changing instead of editing xml

[WIP] hiring warriors

[WIP] different types of warriors and houses

[WIP] resource trading in the market

[WIP] many cities

[WIP] conquering ('Juliusz Cezar')

[WIP] in-game ascii worldmap

[WIP] ascii animations

[WIP] evolution

### Hacking

You don't need any hacked clients to do some hackerman stuff here.

To change the costs and waiting times, edit conf/costs.conf file;

To add/remove resources and enable/disable building in your city, look at game/saves/name_of_game.smg

### Credits

Me (glowiak) for the game itself, libraries, english and Polish translations and pre-made configuration files.

[FIGlet](http://figlet.org) for the ascii logo

ShellMMO is registered trademark of no one.

### Forking, contributing etc

Feel free to fork, or contribute if you want.

PRs are welcome!!!
